# Programme type
![REAC_CDA_V03.svg](./REAC_CDA_V03.svg)


## Démarrage avec des ateliers sur la partie "base" du contenu
- positionnement
- ajuster en fonction des retours

## Préparation du projet fil rouge
- déterminer le projet
- planification et démarrage des conceptions (**C9 & C10**)

## Mise en place du backend
- BDD et ORM (**C6 & C7 & C8**)
- controlleurs REST (**C5 & C12**)
- logique métier à déterminer en fonction du projet (**C11**)
- test unitaires (**C14**)
- CI / CD avec Gitlab (**C14 & C15**)

## Application client Web en VueJS (**C4**)
- Maquettage (**C1**)
- MVC (**C12**)
- Utilisation du Store (**C3 & C12**)
- Utilisation du routeur (**C12**)
- exportation sous forme desktop avec Electron (**C2**)
- utilisation d'un UIKit complet (**C2**)

## Application mobile en ReactNative (**C13**)
- Maquettage (**C1**)
- MVC (**C12**)
- Utilisation du routeur (**C12**)
- utilisation d'un UIKit dédié (**C13**)

---

# Planning prévisionnel

```mermaid
gantt
    title du 24/01/22 au 24/02/23
    dateFormat  YYYY-MM-DD
	axisFormat  %m/%y
	excludes Saturday, Sunday
	
    section Découverte
	Démarrage           :startup, 2022-01-24, 5d
    Conception          :conception, 2022-02-14,  60d
    Backend             :back, 2022-03-14, 120d
	Front Web           :web, 2022-06-13, 120d
	Front Mobile        :mobile, 2022-09-26, 100d
	Rapports            :reports, 2022-11-25, 60d
	Examens             : milestone, m2, 2023-02-22, 3d
```

---

# Contenu abordable
## Base
### Structure de données
- types scalaires standards (int / float / double / boolean /...)
- Array / List / Deque / Heap (FIFO - LIFO)
- Arbre et graphe
- Notions de complexité des algorithmes sur les conteneurs

Ressources :
- [Tutorial FR - Quatre méthodes de recherche dans les tableaux en JavaScript](https://www.digitalocean.com/community/tutorials/js-array-search-methods-fr)
- [Tutorial FR - Comment utiliser .map() pour Iterate via Array Items dans JavaScript](https://www.digitalocean.com/community/tutorials/4-uses-of-javascripts-arraymap-you-should-know-fr)
- [Tutorial FR - Comprendre les objects Map et Set en JavaScript](https://www.digitalocean.com/community/tutorials/understanding-map-and-set-objects-in-javascript-fr)

### Notions de réseau
- outils de debug (ping curl telnet nslookup)
- Adresse IP / Masque de sous réseau / routage
	- [Course FR complete - Les bases du réseau : TCP/IP, IPv4 et IPv6](https://www.it-connect.fr/cours/les-bases-du-reseau-tcpip-ipv4-et-ipv6/)
- DNS
	- [Guide FR basic - Les bases du DNS](https://ilearned.eu.org/les-bases-du-dns.html)
- SSL & PKI
	- [Guide FR complete - SSL / TLS 101 pour les débutants](https://geekflare.com/fr/tls-101/)
- autre
    - Couche du modèle OSI : [Qu'est-ce que le modèle OSI ?](https://www.cloudflare.com/fr-fr/learning/ddos/glossary/open-systems-interconnection-model-osi/)
	- [Guide FR basic - Le fonctionnement d'HTTP](https://ilearned.eu.org/http.html)
	- [Guide FR basic - Comprendre le protocole TCP](https://ilearned.eu.org/tcp.html)
    - Différent mode de discussion : connexion stateless vs statefull (ex: FTP vs HTTP)


### Outillage
- ligne de commande
	- [Mémo : Commandes de base Linux](https://www.linuxtricks.fr/wiki/memo-commandes-de-base-linux)
	- [Guide EN - Learn Enough Command Line to Be Dangerous](https://www.learnenough.com/command-line-tutorial)
- git / gitlab
	- [Guide FR complete - Pro Git](https://git-scm.com/book/fr/v2)
- SSH
	- [Comprendre et maîtriser SSH](https://www.it-connect.fr/cours/comprendre-et-maitriser-ssh/)
- docker & docker-compose
	- [Tutorial FR complete - Docker : découverte des bases](https://www.codeheroes.fr/2021/06/14/docker-decouverte-des-bases/?)
	- [Tutorial FR - Reverse Proxy Traefik avec Docker Compose et Docker Swarm](https://juliensalinas.com/fr/reverse-proxy-traefik-docker-compose-docker-swarm-nlpcloud/)
- debugger
- profiler
- vagrant
- ansible
	- [Tutorial FR - Découverte d'Ansible par un](https://www.corentin-hatte.eu/blog/decouverte-ansible-par-un-nul)
- test unitaires
- CI & CD
	- [Turorial EN - Docker, Kaniko, Buildah](https://itnext.io/docker-kaniko-buildah-209abdde5f94)
	- [Tutorial FR - Builder simplement des images docker avec gitlab-ci (sans dind)](https://blog.revolve.team/2021/07/20/build-images-docker-gitlab-ci-sans-dind/)

### Languages
- POO
- Notions de programmation fonctionnelle
    - [Guide FR - Programmation fonctionnelle en JavaScript](https://buzut.net/programmation-fonctionnelle-en-javascript/)
    - immutability
    - iterators
    - pattern matching


## Conception et gestion de projet

### UML

[What is UML - Visual Paradigm (EN)](https://www.visual-paradigm.com/guide/uml-unified-modeling-language/what-is-uml/)
- Use case
- Diagramme de classe
    - Différents types de relation [Aggregagtion vs Composition](https://www.visual-paradigm.com/guide/uml-unified-modeling-language/uml-aggregation-vs-composition/)
- Diagramme de séquence

### Outils
- Markdown
- Mermaid / PlantUML / draw.io

## Architecture & patterns

- MVC
- REST
- SOA

## Bonnes pratiques
- Review de code pour maîtriser ses commits
- [Tutorial FR - SOLID : les 5 premiers principes de conception orientée objet](https://www.digitalocean.com/community/conceptual_articles/s-o-l-i-d-the-first-five-principles-of-object-oriented-design-fr)
- Clean code & Design patterns : [Refactoring Guru](https://refactoring.guru/fr)
- Early exits

## UI
- [Tutorial FR - Comment utiliser l'API Fetch de JavaScript pour récupérer des données](https://www.digitalocean.com/community/tutorials/how-to-use-the-javascript-fetch-api-to-get-data-fr)

### VueJS pour Web et Desktop (avec Electron)
- [Guides EN](https://v3.vuejs.org/guide/introduction.html)
- [Guides FR (trad tjs en cours au 2021-10-08)](https://vue3-fr.netlify.app/guide/introduction.html)

### ReactNative pour Mobile
- [Tutorial EN - basic](https://reactnative.dev/docs/tutorial)
- [Guides EN](https://reactnative.dev/docs/getting-started)


## Web backend
### PHP Vanilla / Symfony / Laravel
- [Tutoral EN - Full vanilla implementation](https://blog.learningdollars.com/2020/07/08/how-to-build-a-rest-api-using-php/)
- [Tutorial FR - APIPlatform @Kaherecode](https://www.kaherecode.com/tutorial/developper-une-api-rest-avec-symfony-et-api-platform)
- [Tutorial FR - Créer une API JSON avec Laravel](https://www.akilischool.com/cours/laravel-creer-une-api-json)

### Node & Express
- [Guides FR](https://expressjs.com/fr/starter/hello-world.html)

## Persistence

- Composants BDD : view / stored procedure / trigger
- Transactions : 
    - [PHP | Transactions and Concurrency](https://www.doctrine-project.org/projects/doctrine-orm/en/2.10/reference/transactions-and-concurrency.html#transactions-and-concurrency)
    -  [Bien débuter avec les transactions SQL](https://makina-corpus.com/devops/bien-debuter-avec-les-transactions-sql)
- Piliers SGBDR : [Propriétés ACID](https://fr.wikipedia.org/wiki/Propri%C3%A9t%C3%A9s_ACID)
- SQL & NoSQL : [Théorème de CAP](https://openclassrooms.com/fr/courses/4462426-maitrisez-les-bases-de-donnees-nosql/4462471-maitrisez-le-theoreme-de-cap)
