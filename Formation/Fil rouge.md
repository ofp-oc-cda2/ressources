# Etape 1 - Préparer le projet

## Objectifs

Le premier objectif est de définir votre projet de formation : trouver une idée d'application qui vous intéresse ou vous tiens à coeur et commencer à la concevoir.

L'application doit permettre de produire :
- une partie web (backoffice / administration)
- une partie desktop (possibilité de réutiliser la partie Web si elle est écrite en utilisant VueJS ou React)
- une partie application mobile (pour l'utilisateur final)

## Planification
- Courant première semaine (26-28 janvier 2022) : démarrer la réflexion sur le choix de l'application
- Courant deuxième semaine (14-18 février 2022) : découverte des outils et différents types de diagramme
- Fin de deuxième semaine (18 février 2022) : avoir produit une première version propre des conceptions (voir section livrables)

## Livrables

### Cas d'utilisations
- identifier les types d'utilisateurs
	- identifier les fonctionnalités des différents types d'utilisateurs

Formalisme : liste textuelle / diagramme use case / user story

### Modèle de données
- poser les grandes ligne de la structure des données

Formalisme : diagramme de classe, MCD

### Spécifications fonctionnelles
- identifier les contraintes fonctionnelles du projet

Formalisme : textuel

### Maquette

- Première ébauche pour le maquettage de l'interface

Outils : AdobeXD / Figma / ...