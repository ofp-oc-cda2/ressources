# Ressources

Ce dépôt sert à coordonner les ressources pour le groupe de formation, vous y trouverez :
- les grandes lignes du [programme de formation](./Formation/Programme.md)
- un suivi des objectifs du [projet fil rouge](./Formation/Fil rouge.md)
- des [notes / présentations](#présentations) sur des outils / technologies

## Objectifs

- documenter sa pratique : constuire une base de connaissances, garder une trace des sources
- collaborer à un dépôt git : utilisation d'un flux merge request

## Fonctionnement du dépôt

Le dépôt est configuré de façon à forcer les merge requests, pour collaborer la procédure est la suivante :

- cloner le dépôt sur votre machine (`git clone`) ou récupérer les dernières modifications (`git pull`)
- travailler dans une branche
- lorsque vous êtes satisfait pousser votre branche : `git push origin __MY_BRACNCH__`
- faire une demande de fusion / merge request
	- soit utiliser le lien affiché par git lors du push de la branche
	- soit la créer directement dans Gitlab depuis le menu "Merge requests"

# Présentations

## Technos

- [NoSQL avec MongoDB](./Outils/MongoDB.md)

## Outils

- Pour tester les API : [Insomnia](./Outils/Insomnia.md) 
