# MongoDB
![](https://upload.wikimedia.org/wikipedia/commons/thumb/9/93/MongoDB_Logo.svg/2560px-MongoDB_Logo.svg.png)

## Introduction
MongoDB est une base de données NoSQL orientée document. Elle se distingue des bases de données relationnelles par sa flexibilité et ses performances.
Contrairement à une base de données relationnelle SQL traditionnelle, MongoDB ne repose pas sur des tableaux et des colonnes. Les données sont stockées sous forme de collections et de documents.

En général les documents ont une structure de : valeurs / clé : 
![](https://docs.mongodb.com/manual/images/crud-annotated-document.bakedsvg.svg)

#### Avantages :
- Les documents n’ont pas de schéma prédéfini et des champs peuvent être ajoutés à volonté.
- structure clé / valeur facile a comprendre
- Scalabilité bien plus simple qu'avec du SQL
- Le JSON peut exprimer les données en NOSQL, ce qui rend le développement plus simple vu qu'il est supportés par beaucoup de langagues de programations

#### SQL / NOSQL
Vous connaissez surement le SQL avec ses relations.
MongoDB donc le NOSQL n'as pas de relations en tant que tel mais plutot des collections. 
Une collection est un groupe de documents MongoDB et correspond a un tableau sur une base de données relationnelle.

#### Pourquoi le MongoDB et le NOSQL

- MongoDB présente plusieurs avantages majeurs. Tout d’abord, cette base de données NoSQL orientée document se révèle très flexible et adaptée aux cas d’usage concrets d’une entreprise.
- Pas de phpMyAdmin, pas de LAMP, pas de problème de jointures et de MCD a proprement parler (si cela ne vous convaint pas...)
- Les bases théoriques sur les différences fondamentales des modèle SQL et NoSQL : [Openclassrooms - Maitrisez le théorème de CAP](https://openclassrooms.com/fr/courses/4462426-maitrisez-les-bases-de-donnees-nosql/4462471-maitrisez-le-theoreme-de-cap)

### Lien utiles

- MongoDB doc :
  - https://docs.mongodb.com/

- Modélisation NoSQL avec MongoDB :
  - https://docs.mongodb.com/guides/server/introduction/
  - https://docs.mongodb.com/manual/core/data-modeling-introduction/
  
- [Youtube EN - MongoDb en 100 secondes](https://www.youtube.com/watch?v=-bt_y4Loofg&ab_channel=Fireship)
- [Youtube EN - MongoDB Crash Course](https://www.youtube.com/watch?v=ofme2o29ngU&t=40s&ab_channel=WebDevSimplified)
    








